# irstea/php-cs-fixer-config

Configuration pour [PHP-CS-Fixer](https://cs.symfony.com/).

**Attention** : à partir de la version 2.0, PHP **7.1** est requis. Utiliser les version 1.x pour 
les versions précédentes de PHP.

### Installation


```shell
composer require --dev irstea/php-cs-fixer-config
```

### Configuration

La configuration se fait dans un fichier `.php_cs.dist` comme avec PHP-CS-Fixer, mais en utilisant la classe `Irstea\CS\Config`.

Exemple :

```php
<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->exclude('var')
    ->files()
    ->name('*.php')
    ->in('.');

$loader = require __DIR__ . '/vendor/autoload.php';
try {
    return Irstea\CS\Config::create()
        ->setRiskyAllowed(true) // recommandé pour les nouveaux projets, à tester avec de vieux projets.
        ->setIndent('    ')
        ->setLineEnding("\n")
        ->setFinder($finder);
} finally {
    //  Décharge le loader pour éviter des conflits de version de classes.
    $loader->unregister();
}
```

Pour plus de détails, cf. https://cs.symfony.com/#usage

### Usage

```shell
vendor/bin/php-cs-fixer fix
```

cf. https://cs.symfony.com/#usage

### En-tête de fichiers

php-cs-fixer peut s'assurer que tous les fichiers PHP commençent par un commentaire fixe. Ceci
est généralement utilisé pour mettre les mentions légales obligatoires (de licence, notamment).

`irstea/php-cs-fixer-config` permet d'utiliser un modèle avec quelques tags :
* `%package%`  est remplacé par le nom du package Composer,
* `%description%`  est remplacé par le description du package Composer,
* `%yearRange%` est remplacé par la période de développement d'années d'parès le dépôt git.

Par défaut, `irstea/php-cs-fixer-config` consulte la licence indiquée dans le fichier `composer.json`
puis cherche un modèle correspondant dans son dossier [headers/](headers/). S'il ne trouve pas,
il utilise un [modèle par défaut](headers/default.txt) qui renvoie au fichier LICENSE qui devrait
se trouver à la racine du projet.

Sinon, un modèle d'en-tête personnamlié en créant un fichier `.docheader` à la racine du projet.

Exemple :

```
%package% - %description%
(c) %yearRange% Irstea <dsi.poleis@irstea.fr>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
```
