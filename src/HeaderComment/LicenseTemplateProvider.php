<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\HeaderComment;

use Irstea\CS\Composer\ComposerPackageInterface;
use Irstea\CS\FileLocator\FileLocator;
use Irstea\CS\FileLocator\FileLocatorInterface;

/**
 * Class LicenseTemplateProvider.
 */
final class LicenseTemplateProvider implements TemplateProviderInterface
{
    /**
     * @var ComposerPackageInterface
     */
    private $composerPackage;

    /**
     * @var FileLocator
     */
    private $fileLocator;

    /**
     * LicenseTemplateProvider constructor.
     */
    public function __construct(
        ComposerPackageInterface $composerPackage,
        FileLocatorInterface $fileLocator = null
    ) {
        $this->composerPackage = $composerPackage;
        $this->fileLocator = $fileLocator ?: new FileLocator(\dirname(__DIR__, 2) . '/headers');
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate(): ?string
    {
        $path = $this->getLicensePath();

        return $path ? file_get_contents($path) : null;
    }

    private function getLicensePath(): ?string
    {
        $licenses = $this->composerPackage->getLicenses();
        if (!$licenses) {
            return $this->fileLocator->locate('proprietary.txt');
        }

        foreach ($licenses as $license) {
            $templatePath = $this->fileLocator->locate("$license.txt");
            if ($templatePath) {
                return $templatePath;
            }
        }

        return $this->fileLocator->locate('default.txt');
    }
}
