<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\HeaderComment;

use Assert\Assertion;

/**
 * Class ChainTemplateProvider.
 */
final class ChainTemplateProvider implements TemplateProviderInterface
{
    /**
     * @var TemplateProviderInterface[]
     */
    private $templateProviders;

    /**
     * ChainTemplateProvider constructor.
     *
     * @param TemplateProviderInterface[] $templateProviders
     */
    public function __construct(array $templateProviders)
    {
        Assertion::allImplementsInterface($templateProviders, TemplateProviderInterface::class);

        $this->templateProviders = $templateProviders;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate(): ?string
    {
        foreach ($this->templateProviders as $templateProvider) {
            $template = $templateProvider->getTemplate();
            if ($template) {
                return $template;
            }
        }

        return null;
    }
}
