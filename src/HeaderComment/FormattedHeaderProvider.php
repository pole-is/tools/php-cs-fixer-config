<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\HeaderComment;

/**
 * Class FormattedHeaderProvider.
 */
final class FormattedHeaderProvider implements HeaderProviderInterface
{
    /**
     * @var TemplateFormatterInterface
     */
    private $templateFormatter;

    /**
     * @var TemplateProviderInterface
     */
    private $templateProvider;

    /**
     * UserDefinedTemplateProvider constructor.
     */
    public function __construct(TemplateProviderInterface $templateProvider, TemplateFormatterInterface $templateFormatter)
    {
        $this->templateProvider = $templateProvider;
        $this->templateFormatter = $templateFormatter;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeader(): ?string
    {
        $template = trim($this->templateProvider->getTemplate());

        return $template ? $this->templateFormatter->format($template) : null;
    }
}
