<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\Git;

use Assert\Assertion;

/**
 * Class GitRepository.
 */
final class GitRepository implements GitRepositoryInterface
{
    /**
     * @var string
     */
    private $repositoryPath;

    /**
     * GitRepository constructor.
     */
    public function __construct(string $repositoryPath)
    {
        Assertion::directory($repositoryPath . '/.git');

        $this->repositoryPath = $repositoryPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeadCommit(): string
    {
        return trim(shell_exec('git -C ' . escapeshellarg($this->repositoryPath) . ' rev-parse HEAD'));
    }

    /**
     * {@inheritdoc}
     */
    public function getYearRange(): string
    {
        $years = [];
        exec(
            sprintf(
                'git -C %s log --format=%%cd --date=format:%%Y --date-order %s | sed \'1p;$p;d\'',
                escapeshellarg($this->repositoryPath),
                escapeshellarg($this->getHeadCommit())
            ),
            $years
        );
        if (!$years) {
            return '???';
        }
        $last = trim(array_shift($years));
        $first = $years ? trim(array_pop($years)) : $last;

        return $last !== $first ? "$first-$last" : $first;
    }
}
