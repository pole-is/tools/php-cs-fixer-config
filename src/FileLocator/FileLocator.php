<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\FileLocator;

/**
 * Class FileLocator.
 */
final class FileLocator implements FileLocatorInterface
{
    /**
     * @var string
     */
    private $baseDir;

    /**
     * FileLocator constructor.
     */
    public function __construct(string $baseDir)
    {
        $this->baseDir = rtrim($baseDir, \DIRECTORY_SEPARATOR) . \DIRECTORY_SEPARATOR;
    }

    /**
     * {@inheritdoc}
     */
    public function locate(string $filename): ?string
    {
        $path = $this->baseDir . ltrim($filename, \DIRECTORY_SEPARATOR);
        if (!file_exists($path)) {
            return null;
        }

        return $path;
    }
}
