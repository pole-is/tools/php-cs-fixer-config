<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\FileLocator;

use Assert\Assertion;

/**
 * Class ChainFileLocator.
 */
final class ChainFileLocator implements FileLocatorInterface
{
    /**
     * @var FileLocatorInterface[]
     */
    private $fileLocators;

    /**
     * ChainFileLocator constructor.
     *
     * @param FileLocatorInterface[] $fileLocators
     */
    public function __construct(array $fileLocators)
    {
        Assertion::allImplementsInterface($fileLocators, FileLocatorInterface::class);

        $this->fileLocators = $fileLocators;
    }

    /**
     * {@inheritdoc}
     */
    public function locate(string $filename): ?string
    {
        foreach ($this->fileLocators as $fileLocator) {
            $result = $fileLocator->locate($filename);
            if ($result) {
                return $result;
            }
        }

        return null;
    }
}
