<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS;

use Irstea\CS\Composer\ComposerPackage;
use Irstea\CS\Composer\ComposerPackageInterface;
use Irstea\CS\FileLocator\FileLocator;
use Irstea\CS\Git\GitRepository;
use Irstea\CS\HeaderComment\ChainTemplateProvider;
use Irstea\CS\HeaderComment\FormattedHeaderProvider;
use Irstea\CS\HeaderComment\HeaderProviderInterface;
use Irstea\CS\HeaderComment\LicenseTemplateProvider;
use Irstea\CS\HeaderComment\SPDXLicenseTemplateProvider;
use Irstea\CS\HeaderComment\TemplateFormatter;
use Irstea\CS\HeaderComment\UserDefinedTemplateProvider;
use PhpCsFixer\Config as PhpCsFixerConfig;

/**
 * Class Config.
 */
final class Config extends PhpCsFixerConfig
{
    /**
     * @var ComposerPackageInterface
     */
    private $composerPackage;

    /**
     * @var HeaderProviderInterface
     */
    private $headerProvider;

    /**
     * @var array<string, mixed>
     */
    private $ruleOverrides = [];

    /**
     * Config constructor.
     */
    public function __construct(
        ComposerPackageInterface $composerPackage,
        HeaderProviderInterface $headerProvider,
        string $name = 'default'
    ) {
        parent::__construct($name);

        $this->composerPackage = $composerPackage;
        $this->headerProvider = $headerProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function getRules(): array
    {
        $phpVersion = $this->composerPackage->getRequiredPHPVersion();
        $risky = $this->getRiskyAllowed();

        return array_replace(
            $this->ruleSets($phpVersion, $risky),
            $this->baseRules($phpVersion, $risky),
            $this->ruleOverrides
        );
    }

    private function ruleSets(float $phpVersion, bool $risky): array
    {
        $sets = [
            '@PSR2'                  => true,
            '@PSR12'                 => true,
            '@Symfony'               => true,
            '@Symfony:risky'         => $risky,
        ];

        $sets['@PHP54Migration'] = $phpVersion >= 5.4;

        $sets['@PHP56Migration'] = $phpVersion >= 5.6;
        $sets['@PHP56Migration:risky'] = $risky && $sets['@PHP56Migration'];

        $sets['@PHP70Migration'] = $phpVersion >= 7.0;
        $sets['@PHP70Migration:risky'] = $risky && $sets['@PHP70Migration'];

        $sets['@PHP71Migration'] = $phpVersion >= 7.1;
        $sets['@PHP71Migration:risky'] = $risky && $sets['@PHP71Migration'];

        $sets['@PHP73Migration'] = $phpVersion >= 7.3;

        $sets['@PHP74Migration'] = $phpVersion >= 7.4;
        $sets['@PHP74Migration:risky'] = $risky && $sets['@PHP74Migration'];

        $sets['@PHP80Migration'] = $phpVersion >= 8.0;

        return $sets;
    }

    public function baseRules(float $phpVersion, bool $risky): array
    {
        $rules = [
            // Configuration && overrides
            'binary_operator_spaces'                    => ['align_double_arrow' => true],
            'blank_line_after_opening_tag'              => false,
            'concat_space'                              => ['spacing' => 'one'],
            'method_argument_space'                     => ['ensure_fully_multiline' => true],
            'declare_strict_types'                      => $phpVersion >= 7.0,
            'blank_line_after_opening_tag'              => false,

            // Safe
            'align_multiline_comment'                   => true,
            'array_syntax'                              => ['syntax' => 'short'],
            'general_phpdoc_annotation_remove'          => ['annotations' => ['author', 'package']],
            'no_multiline_whitespace_before_semicolons' => true,
            'no_useless_else'                           => true,
            'no_useless_return'                         => true,
            'ordered_imports'                           => true,
            'phpdoc_add_missing_param_annotation'       => true,
            'phpdoc_annotation_without_dot'             => true,
            'phpdoc_order'                              => true,
            'semicolon_after_instruction'               => true,
            'yoda_style'                                => false,
        ];

        $header = $this->headerProvider->getHeader();
        if ($header) {
            $rules['header_comment'] = [
                'commentType' => 'comment',
                'location'    => 'after_declare_strict',
                'separate'    => 'bottom',
                'header'      => $header,
            ];
        }

        if ($risky) {
            $rules['is_null'] = ['use_yoda_style' => false];

            if ($phpVersion >= 7.0) {
                $rules['non_printable_character'] = ['use_escape_sequences_in_strings' => true];
            }
        }

        return $rules;
    }

    /**
     * @param string $rule
     * @param mixed  $setting
     */
    public function setRule($rule, $setting): void
    {
        $this->ruleOverrides[$rule] = $setting;
    }

    /**
     * {@inheritdoc}
     */
    public function setRules(array $rules): void
    {
        $this->ruleOverrides = array_replace($this->ruleOverrides, $rules);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(): self
    {
        $stackTrace = debug_backtrace(1);
        $callerPath = \dirname($stackTrace[0]['file']);

        $fileLocator = new FileLocator($callerPath);
        $composerPackage = new ComposerPackage($fileLocator);

        $variables = self::getTemplateVariables($callerPath, $composerPackage);

        $headerProvider = new FormattedHeaderProvider(
            new ChainTemplateProvider(
                [
                    new UserDefinedTemplateProvider($fileLocator),
                    new SPDXLicenseTemplateProvider($composerPackage),
                    new LicenseTemplateProvider($composerPackage),
                ]
            ),
            new TemplateFormatter($variables)
        );

        return new self($composerPackage, $headerProvider);
    }

    private static function getTemplateVariables(string $path, ComposerPackage $composerPackage): array
    {
        $gitRepository = self::findGitRepository($path);

        return [
            '%copyrightHolder%' => 'INRAE',
            '%package%'         => $composerPackage->getName(),
            '%description%'     => $composerPackage->getDescription(),
            '%yearRange%'       => $gitRepository ? $gitRepository->getYearRange() : date('Y'),
        ];
    }

    private static function findGitRepository(string $path): ?GitRepository
    {
        while ($path !== '/') {
            if (is_dir($path . '/.git')) {
                return new GitRepository($path);
            }

            $path = \dirname($path);
        }

        return null;
    }
}
