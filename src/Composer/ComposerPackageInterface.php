<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\Composer;

/**
 * Interface ComposerPackageInterface.
 */
interface ComposerPackageInterface
{
    public function getName(): string;

    public function getDescription(): string;

    public function getRequiredPHPVersion(): float;

    public function getLicenses(): iterable;
}
