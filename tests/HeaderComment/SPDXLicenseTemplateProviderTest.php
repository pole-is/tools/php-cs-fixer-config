<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\Tests\HeaderComment;

use Irstea\CS\Composer\ComposerPackageInterface;
use Irstea\CS\HeaderComment\SPDXLicenseTemplateProvider;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * Class SPDXLicenseTemplateProviderTest.
 */
final class SPDXLicenseTemplateProviderTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var ComposerPackageInterface|ObjectProphecy
     */
    private $composerPackage;

    /**
     * @var SPDXLicenseTemplateProviderTest
     */
    private $templateProvider;

    protected function setUp(): void
    {
        $this->root = vfsStream::setup('licenses');

        $this->composerPackage = $this->prophesize(ComposerPackageInterface::class);

        $this->templateProvider = new SPDXLicenseTemplateProvider(
            $this->composerPackage->reveal(),
            $this->root->url() . '/%s.json'
        );
    }

    public function testKnownLicense(): void
    {
        $this->composerPackage->getLicenses()
            ->shouldBeCalled()
            ->willReturn(['foo']);

        $this->root->addChild(vfsStream::newFile('foo.json')->setContent('{"standardLicenseHeader": "bar"}'));

        $result = $this->templateProvider->getTemplate();

        self::assertEquals('bar', $result);
    }

    public function testUnknownLicense(): void
    {
        $this->composerPackage->getLicenses()
            ->shouldBeCalled()
            ->willReturn(['foo']);

        $result = $this->templateProvider->getTemplate();

        self::assertNull($result);
    }

    public function testInvalidJSON(): void
    {
        $this->composerPackage->getLicenses()
            ->shouldBeCalled()
            ->willReturn(['foo']);

        $this->root->addChild(vfsStream::newFile('foo.json')->setContent('bar'));

        $result = $this->templateProvider->getTemplate();

        self::assertNull($result);
    }
}
