<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\Tests\HeaderComment;

use Irstea\CS\Composer\ComposerPackageInterface;
use Irstea\CS\FileLocator\FileLocator;
use Irstea\CS\FileLocator\FileLocatorInterface;
use Irstea\CS\HeaderComment\LicenseTemplateProvider;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * Class LicenseTemplateProviderTest.
 */
final class LicenseTemplateProviderTest extends TestCase
{
    /**
     * @var ComposerPackageInterface|ObjectProphecy
     */
    private $composerPackage;

    /**
     * @var vfsStreamDirectory
     */
    private $filesystem;

    /**
     * @var FileLocatorInterface
     */
    private $fileLocator;

    /**
     * @var LicenseTemplateProvider
     */
    private $templateProvider;

    protected function setUp(): void
    {
        parent::setUp();

        /* @var ComposerPackageInterface|ObjectProphecy $composerPackage */
        $this->composerPackage = $this->prophesize(ComposerPackageInterface::class);

        $this->filesystem = vfsStream::setup(
            'root',
            0755,
            [
                'proprietary.txt' => 'proprietary-template',
                'GPL.txt'         => 'GPL-template',
                'default.txt'     => 'default-template',
            ]
        );

        $this->fileLocator = new FileLocator($this->filesystem->url());

        $this->templateProvider = new LicenseTemplateProvider(
            $this->composerPackage->reveal(),
            $this->fileLocator
        );
    }

    public function testShouldProvideKnownLicenseHeader(): void
    {
        $this->composerPackage->getLicenses()
            ->shouldBeCalled()
            ->willReturn(['MIT', 'GPL']);

        self::assertEquals('GPL-template', $this->templateProvider->getTemplate());
    }

    public function testShouldProvideDefaultLicenseHeader(): void
    {
        $this->composerPackage->getLicenses()
            ->shouldBeCalled()
            ->willReturn(['MIT']);

        self::assertEquals('default-template', $this->templateProvider->getTemplate());
    }

    public function testShouldProvideProprietaryLicenseHeaderWithNoLicense(): void
    {
        $this->composerPackage->getLicenses()
            ->shouldBeCalled()
            ->willReturn([]);

        self::assertEquals('proprietary-template', $this->templateProvider->getTemplate());
    }
}
