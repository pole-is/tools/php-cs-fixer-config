<?php
declare(strict_types=1);
/*
 * irstea/php-cs-fixer-config - Jeux de règles pour php-cs-fixer.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Irstea\CS\Tests\FileLocator;

use Irstea\CS\FileLocator\FileLocator;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

/**
 * Class FileLocatorTest.
 */
class FileLocatorTest extends TestCase
{
    public function testShouldLocateExistingFile(): void
    {
        $fs = vfsStream::setup('root', 0755, [
            'file' => 'content',
        ]);

        $locator = new FileLocator($fs->url());

        self::assertEquals(
            $fs->getChild('file')->url(),
            $locator->locate('file')
        );
    }

    public function testShouldReturnNullOnMissingFile(): void
    {
        $fs = vfsStream::setup();
        $locator = new FileLocator($fs->url());

        self::assertNull($locator->locate('file'));
    }
}
